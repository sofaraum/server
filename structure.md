# .

#### Structs

* Users (Admins)
  * Standard...
* Rooms
  * ID
  * Name
* Clients
  * ID
  * Name
  * Pubkey
* Stats
  * ID
  * Count
  * ClientID
  * RoomID

Client signiert seinen Request mit seinem Privaten Schlüssel und der Server verifiziert das dann.
Mit ed25519

Ein neuer Client wird registriert, indem er seinen pubkey mit name an den Server schickt 
(hinter Authentifizierung).