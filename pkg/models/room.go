//   Sofaraum server is the server which collects the statistics
//   for the sofaraum-heatmap application.
//   Copyright 2018 K.Langenberg and contributors. All rights reserved.
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <https://www.gnu.org/licenses/>.

package models

type Room struct {
	ID   int64  `xorm:"int(11) autoincr not null unique pk" json:"id"`
	Name string `xorm:"varchar(250)" json:"name"`

	Created int64 `xorm:"created" json:"created"`
	Updated int64 `xorm:"updated" json:"updated"`
}

func (Room) TableName() string {
	return "rooms"
}
