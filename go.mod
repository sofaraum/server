module code.sofaraum.de/server

require (
	cloud.google.com/go v0.34.0 // indirect
	code.vikunja.io/web v0.0.0-20181130231148-b061c20192fb
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/asaskevich/govalidator v0.0.0-20180720115003-f9ffefc3facf
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/garyburd/redigo v1.6.0 // indirect
	github.com/go-openapi/jsonreference v0.17.2 // indirect
	github.com/go-openapi/spec v0.17.2 // indirect
	github.com/go-sql-driver/mysql v1.4.1
	github.com/go-xorm/core v0.6.0
	github.com/go-xorm/tests v0.5.6 // indirect
	github.com/go-xorm/xorm v0.7.1
	github.com/go-xorm/xorm-redis-cache v0.0.0-20180727005610-859b313566b2
	github.com/google/go-cmp v0.2.0 // indirect
	github.com/labstack/echo v3.3.5+incompatible
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/spf13/viper v1.3.0
	github.com/swaggo/echo-swagger v0.0.0-20180315045949-97f46bb9e5a5
	github.com/swaggo/files v0.0.0-20180215091130-49c8a91ea3fa // indirect
	github.com/swaggo/gin-swagger v1.0.0 // indirect
	github.com/swaggo/swag v1.4.0
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9
	golang.org/x/net v0.0.0-20181201002055-351d144fa1fc // indirect
	golang.org/x/tools v0.0.0-20181205224935-3576414c54a4 // indirect
	google.golang.org/appengine v1.3.0 // indirect
)
