//  Sofaraum server is the server which collects the statistics
//  for the sofaraum-heatmap application.
//  Copyright 2018 K.Langenberg and contributors. All rights reserved.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"code.sofaraum.de/server/docs"
	"code.sofaraum.de/server/pkg/config"
	"code.sofaraum.de/server/pkg/log"
	"code.sofaraum.de/server/pkg/models"
	"code.sofaraum.de/server/pkg/routes"
	"context"
	"github.com/spf13/viper"
	"os"
	"os/signal"
	"time"
)

// Version sets the version to be printed to the user. Gets overwritten by "make release" or "make build" with last git commit or tag.
var Version = "0.1"

func main() {

	// Init logging
	log.InitLogger()

	// Init Config
	err := config.InitConfig()
	if err != nil {
		log.Log.Fatal(err.Error())
		os.Exit(1)
	}

	// Set Engine
	err = models.SetEngine()
	if err != nil {
		log.Log.Fatal(err.Error())
		os.Exit(1)
	}

	// Version notification
	log.Log.Infof("Sofaraum version %s", Version)

	// Additional swagger information
	docs.SwaggerInfo.Version = Version

	// Create first admin if needed
	firstAdmin, err := models.CreateFirstAdmin()
	if err != nil {
		log.Log.Fatal("Could not create first admin.", err)
		os.Exit(1)
	}
	if firstAdmin != nil {
		log.Log.Infof("Created first admin user with name %s", firstAdmin.Username)
	}

	// Start the webserver
	e := routes.NewEcho()
	routes.RegisterRoutes(e)
	// Start server
	go func() {
		if err := e.Start(viper.GetString("service.interface")); err != nil {
			e.Logger.Info("shutting down...")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	log.Log.Infof("Shutting down...")
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}
